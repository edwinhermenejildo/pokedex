package com.example.pokedex.datamodel.gson.utils;

import android.content.Context;

import androidx.annotation.NonNull;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.example.pokedex.R;

/**
 * @author Edwin Hermenejildo Reyes
 * @version 2.0
 * Created on: 08/04/2017
 * E-mail: edwinhermenjildo@gmail.com
 */

public class DialogManager {
    public static MaterialDialog showErrorDialog(Context context, String message) {
        return new MaterialDialog.Builder(context)
                .title(R.string.error)
                .content(message)
                .autoDismiss(false)
                .cancelable(false)
                .positiveText(R.string.button_aceptar)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    public static MaterialDialog showAlertDialog(Context context, String message) {
        return new MaterialDialog.Builder(context)
                .title(R.string.alert)
                .content(message)
                .autoDismiss(false)
                .cancelable(false)
                .positiveText(R.string.button_aceptar)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    public static MaterialDialog showDialog(Context context, String title, String message) {
        return new MaterialDialog.Builder(context)
                .title(title)
                .content(message)
                .autoDismiss(false)
                .cancelable(false)
                .positiveText(R.string.button_aceptar)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    public static MaterialDialog.Builder createDialog(Context context, String title, String message) {
        return new MaterialDialog.Builder(context)
                .title(title)
                .content(message)
                .autoDismiss(false)
                .cancelable(false);
    }


    public static MaterialDialog.Builder showLoadingDialog(Context context) {
        return new MaterialDialog.Builder(context)
                .title(R.string.please_wait)
                .content(R.string.load_content)
                .autoDismiss(false)
                .progress(true, 0)
                .cancelable(false);
    }

    public static MaterialDialog.Builder createLoadingDialog(Context context, String title, String message) {
        return new MaterialDialog.Builder(context)
                .title(title)
                .content(message)
                .autoDismiss(false)
                .progress(true, 0)
                .cancelable(false);
    }
}
