package com.example.pokedex.datamodel.gson.adapter;

import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pokedex.R;
import com.example.pokedex.datamodel.gson.PokedexResponse;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder> {

    public interface OnNotificationClickListener {
        void onNotificationClick(String name, String url);
    }

    private Context mContext;
    private ArrayList<String> mDataset;
    AdapterView.OnItemClickListener onItemClickListener;

    @Override
    public ViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.itemlist_pokemon_image, parent, false);
        return new ViewHolder(view);
    }

    public ImageAdapter(Context context, ArrayList<String> dataset) {
        this.mContext = context;
        this.mDataset = dataset;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setProfileImage(mDataset.get(position));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.list_item_image)       ImageView imageView;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void setProfileImage (String url) {
            if (url.trim().isEmpty()) {
                imageView.setBackground(ContextCompat.getDrawable(mContext, R.color.mdtp_transparent_black));
            } else {
                Picasso.with(mContext).load(url).placeholder(R.color.mdtp_transparent_black).error(R.color.mdtp_transparent_black).into(imageView);
            }
        }

    }

    public void updateDataset(ArrayList<String> notificationData) {
        mDataset.clear();
        mDataset.addAll(notificationData);
        notifyDataSetChanged();
    }
}
