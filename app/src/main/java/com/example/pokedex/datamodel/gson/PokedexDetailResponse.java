package com.example.pokedex.datamodel.gson;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PokedexDetailResponse {
    @SerializedName("abilities") private List<Abilities> listAbilities;
    @SerializedName("base_experience")  private String baseExperience;
    @SerializedName("sprites")  private Spirites sprites;
    @SerializedName("types")  private List<Types> listTypes;
    @SerializedName("weight")  private Integer weight;

    public List<Abilities>getListAbilities() {
        return listAbilities;
    }

    public void setListAbilities(List<Abilities> listAbilities) {
        this.listAbilities = listAbilities;
    }

    public String getBaseExperience() {
        return baseExperience;
    }

    public void setBaseExperience(String baseExperience) {
        this.baseExperience = baseExperience;
    }

    public Spirites getSprites(){return sprites;}

    public void setSprites(Spirites sprites){this.sprites = sprites;}

    public List<Types>getListTypes() {
        return listTypes;
    }

    public void setListTypes(List<Types> listTypes) {
        this.listTypes = listTypes;
    }

    public Integer getWeight(){return weight;}

    public void setWeight(Integer weight){this.weight = weight;}

    public class Abilities{
        @SerializedName("ability") private Ability ability;
        @SerializedName("is_hidden")  private String is_hidden;
        @SerializedName("slot")  private String slot;

        public Ability getAbility(){return ability;}
        public void setAbility(Ability ability){ this.ability = ability;}

        public String getIs_hidden(){return is_hidden;}
        public void setIs_hidden(String is_hidden){this.is_hidden = is_hidden;}

        public String getSlot(){return slot;}
        public void setSlot(String slot){this.slot = slot;}
    }

    public class Ability{
        @SerializedName("name") private String name;
        @SerializedName("url")  private String url;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }
    public class Types{
        @SerializedName("slot") private String slot;
        @SerializedName("type") private Type type;

        public String getSlot() {
            return slot;
        }
        public void setSlot(String slot) {
            this.slot = slot;
        }

        public Type getType(){return type;}
        public void setType(Type type){ this.type = type;}
    }

    public class Type{
        @SerializedName("name") private String name;
        @SerializedName("url")  private String url;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }

    public class Spirites{
        @SerializedName("back_default") private String backDefault;
        @SerializedName("back_shiny")  private String backShiny;
        @SerializedName("front_default") private String frontDefault;
        @SerializedName("front_shiny")  private String frontShiny;

        public String getBackDefault() {
            return backDefault;
        }

        public void setBackDefault(String backDefault) {
            this.backDefault = backDefault;
        }

        public String getBackShiny() {
            return backShiny;
        }

        public void setBackShiny(String backShiny) {
            this.backShiny = backShiny;
        }

        public String getFrontDefault() {
            return frontDefault;
        }

        public void setFrontDefault(String frontDefault) {
            this.frontDefault = frontDefault;
        }

        public String getFrontShiny() {
            return frontShiny;
        }

        public void setFrontShiny(String frontShiny) {
            this.frontShiny = frontShiny;
        }
    }
}
