package com.example.pokedex.datamodel.gson.adapter;

import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pokedex.R;
import com.example.pokedex.datamodel.gson.PokedexResponse;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PokedexAdapter extends RecyclerView.Adapter<PokedexAdapter.ViewHolder> {

    public interface OnNotificationClickListener {
        void onNotificationClick(String name, String url);
    }

    private Context mContext;
    private List<PokedexResponse.Result> mDataset;
    private OnNotificationClickListener mOnNotificationClickListener;

    @Override
    public ViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.itemlist_pokemon, parent, false);
        return new ViewHolder(view);
    }

    public PokedexAdapter(Context context, ArrayList<PokedexResponse.Result> dataset, OnNotificationClickListener listener) {
        this.mContext = context;
        this.mDataset = dataset;
        this.mOnNotificationClickListener = listener;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Log.d("TAG", "onBindViewHolder() position: " + position);
        holder.setTxtID(position + 1);
        holder.setTxtName(mDataset.get(position).getName());
        holder.setProfileImage(mDataset.get(position).getUrl());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnNotificationClickListener.onNotificationClick(mDataset.get(holder.getAdapterPosition()).getName(),mDataset.get(holder.getAdapterPosition()).getUrl());

            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textView_id)         TextView txtID;
        @BindView(R.id.textView_name)        TextView txtName;
        @BindView(R.id.imageView_pokedex)       ImageView imgPokedex;
        @BindView(R.id.button_pokedex)         Button btnPokedex;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void setProfileImage (String url) {
            if (url.trim().isEmpty()) {
                imgPokedex.setBackground(ContextCompat.getDrawable(mContext, R.color.mdtp_transparent_black));
            } else {
                String[] parts = url.split("pokemon");
                String indice = parts[1].toString().replace("/","");
                String urlImage = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/"+ indice +".png";
                Picasso.with(mContext).load(urlImage).placeholder(R.color.mdtp_transparent_black).error(R.color.mdtp_transparent_black).into(imgPokedex);
            }
        }

        void setTxtName(String message) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                txtName.setText(Html.fromHtml("Nombre: "+ message, Html.FROM_HTML_MODE_COMPACT));
            } else {
                txtName.setText(Html.fromHtml("Nombre: " + message));
            }
        }

        void setTxtID(int message) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                txtID.setText(Html.fromHtml("ID: " + String.valueOf(message), Html.FROM_HTML_MODE_COMPACT));
            } else {
                txtID.setText(Html.fromHtml("ID: " + String.valueOf(message)));
            }
        }

    }

    public void updateDataset(List<PokedexResponse.Result> notificationData) {
        mDataset.clear();
        mDataset.addAll(notificationData);
        notifyDataSetChanged();
    }
}
