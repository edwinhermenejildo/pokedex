package com.example.pokedex.datamodel.gson;

import com.google.gson.annotations.SerializedName;

public class MetaResponse {
    @SerializedName("code") private int code;
    @SerializedName("message") private String message;

    public MetaResponse() {

    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
