package com.example.pokedex.datamodel.gson.objects;

import com.example.pokedex.datamodel.gson.ErrorResponse;
import com.example.pokedex.datamodel.gson.MetaResponse;

import java.util.ArrayList;
import java.util.List;

public class APIError {
    private MetaResponse meta;
    private List<ErrorResponse> errors;

    public APIError() {

    }

    public APIError(int code, String message, List<ErrorResponse> errorList) {
        this.meta = new MetaResponse();
        this.errors = new ArrayList<>();

        this.meta.setCode(code);
        this.meta.setMessage(message);
        this.errors.addAll(errorList);
    }

    public MetaResponse getMetaResponse() {
        return meta;
    }

    public int getStatusCode() {
        return meta.getCode();
    }

    public String getStatusMessage() {
        return meta.getMessage();
    }

    public List<ErrorResponse> errorList() {
        return errors;
    }
}
