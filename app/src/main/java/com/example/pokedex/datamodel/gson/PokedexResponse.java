package com.example.pokedex.datamodel.gson;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PokedexResponse {
    @SerializedName("meta")   private Meta   meta;
    @SerializedName("results") private List<Result> listadoResult;

    public List<Result>getListadoResult() {
        return listadoResult;
    }

    public void setListadoResult(List<Result> listadoResult) {
        this.listadoResult = listadoResult;
    }

    public class Meta {
        @SerializedName("count")                   private int count;
        @SerializedName("next")                private String next;
        @SerializedName("previous")                private String previous;

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public String getNext() {
            return next;
        }

        public void setNext(String next) {
            this.next = next;
        }

        public String getPrevious() {
            return previous;
        }

        public void setPrevious(String previous) {
            this.previous = previous;
        }
    }

    public class Result {
        @SerializedName("name") private String name;
        @SerializedName("url")  private String url;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }

}
