package com.example.pokedex.apiclient;

import android.util.Log;
import android.util.MalformedJsonException;

import com.example.pokedex.datamodel.gson.objects.APIError;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by edwin on 10/24/2023.
 */

public abstract class RetrofitCallback<T> implements Callback<T> {
    private static final String TAG = "Retrofit Callback";

    protected RetrofitCallback() {
        onStart();
    }

    public final void onResponse(Call<T> call, Response<T> response) {
        try {
            if (response.isSuccessful()) {
                onRequestSuccess(call, response);
            } else {
                APIError error = RetrofitClient.parseHttpError(response);
                if (error.getMetaResponse() == null) {
                    Log.e(TAG, "APIError.metaResponse() is null, Json malformed. ");
                    throw new MalformedJsonException("APIError Response null or malformed");
                } else {
                    onRequestFailure(call, error);
                }
            }
        } catch (Exception e) {
            onRequestError(call, e);
        }
        onFinish();
    }

    public final void onFailure(Call<T> call, Throwable t) {
        onRequestError(call, t);
        onFinish();
    }

    public abstract void onStart();

    public abstract void onFinish();

    public abstract void onRequestSuccess(Call<T> call, Response<T> response);

    public abstract void onRequestFailure(Call<T> call, APIError error);

    public abstract void onRequestError(Call<T> call, Throwable t);

}