package com.example.pokedex.apiclient;
import java.lang.annotation.Annotation;

import com.example.pokedex.apiclient.interceptors.JsonInterceptor;
import com.example.pokedex.apiclient.interceptors.RequestInterceptor;
import com.example.pokedex.datamodel.gson.ErrorResponse;
import com.example.pokedex.datamodel.gson.objects.APIError;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by edwin on 10/24/2023.
 */

public class RetrofitClient {
    private static RetrofitClient mInstance = null;
    private static Retrofit.Builder mRetrofitBuilder = null;

    public static RetrofitClient getInstance() {
        if (mInstance == null) {
            mInstance = new RetrofitClient();
        }
        return mInstance;
    }

    private RetrofitClient() {
        mRetrofitBuilder = new Retrofit.Builder()
                .baseUrl("https://pokeapi.co/")
                .addConverterFactory(GsonConverterFactory.create());
    }

    static APIError parseHttpError(Response<?> response) throws IOException {
        Converter<ResponseBody, APIError> converter = mRetrofitBuilder.build().responseBodyConverter(APIError.class, new Annotation[0]);
        return converter.convert(response.errorBody());
    }

    public static String buildErrorMessage(APIError error) {
        StringBuilder errorMessage = new StringBuilder();
        errorMessage.append(error.getStatusMessage());
        if (!error.errorList().isEmpty()) {
            errorMessage.append("\n\n");
            for (ErrorResponse errorDetail : error.errorList()) {
                errorMessage.append(errorDetail.getMessage());
                errorMessage.append("\n");
            }
        }
        return "Favor verifique su conexión a internet e intente de nuevo";
    }

    public <S> S setConnection(Class<S> serviceClass) {
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
                .addInterceptor(new JsonInterceptor())
                .addInterceptor(new RequestInterceptor())
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(20, TimeUnit.SECONDS)
                .build();

        return mRetrofitBuilder.client(httpClient).build().create(serviceClass);
    }
}
