package com.example.pokedex.apiclient;

import com.example.pokedex.datamodel.gson.PokedexDetailResponse;
import com.example.pokedex.datamodel.gson.PokedexResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface RestServices {
    @GET("api/v2/pokemon?limit=40")
    Call<PokedexResponse> getPokemon();

    @GET("api/v2/pokemon/{id}/")
    Call<PokedexDetailResponse> getDetailPokemon(@Path("id") int postID);
}
