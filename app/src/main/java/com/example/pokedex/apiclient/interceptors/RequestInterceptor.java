package com.example.pokedex.apiclient.interceptors;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * @author Edwin Hermenejildo Reyes
 * @version 2.0
 * Created on: 08/04/2017
 * E-mail: edwinhermenjildo@gmail.com
 */


public class RequestInterceptor implements Interceptor {
    private static final String TAG = "Request Interceptor";

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request originalRequest = chain.request();

        Request requestBuilder = originalRequest.newBuilder()
                .header("Content-Type", "application/x-www-form-urlencoded")
                .method(originalRequest.method(), originalRequest.body())
                .build();

        return chain.proceed(requestBuilder);
    }
}
