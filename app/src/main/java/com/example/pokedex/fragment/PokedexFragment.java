package com.example.pokedex.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.example.pokedex.R;
import com.example.pokedex.apiclient.RestServices;
import com.example.pokedex.apiclient.RetrofitCallback;
import com.example.pokedex.apiclient.RetrofitClient;
import com.example.pokedex.datamodel.gson.PokedexResponse;
import com.example.pokedex.datamodel.gson.adapter.PokedexAdapter;
import com.example.pokedex.datamodel.gson.objects.APIError;
import com.example.pokedex.datamodel.gson.utils.DialogManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PokedexFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PokedexFragment extends Fragment implements PokedexAdapter.OnNotificationClickListener {

    private static final String TAG = "Pokedex";

    private String imagenPokedex;
    private String ID;
    private String name;

    @BindView(R.id.recycler_pokedex) RecyclerView mRecyclerView;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private Context mContext;
    private PokedexAdapter ePokedexAdapter;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public PokedexFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PokedexFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PokedexFragment newInstance(String param1, String param2) {
        PokedexFragment fragment = new PokedexFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();

        ePokedexAdapter = new PokedexAdapter(mContext, new ArrayList<PokedexResponse.Result>(), this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pokedex, container, false);
        ButterKnife.bind(this, view);

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(ePokedexAdapter);

        return view;
    }

    // TODO: Servicios

    private void cargarPokedexService(final Context context, final Intent intent) {
        final MaterialDialog loadingDialog = DialogManager.showLoadingDialog(getActivity()).build();
        final Handler runLoadingIndicator = new Handler();
        final Runnable showLoadingIndicator = new Runnable() {
            @Override
            public void run() {
                loadingDialog.show();
            }
        };

        RestServices restServices = RetrofitClient.getInstance().setConnection(RestServices.class);
        Call<PokedexResponse> call = restServices.getPokemon();
        call.enqueue(new RetrofitCallback<PokedexResponse>() {
            @Override
            public void onStart() {
                runLoadingIndicator.postDelayed(showLoadingIndicator, 600);
            }

            @Override
            public void onFinish() {
                runLoadingIndicator.removeCallbacks(showLoadingIndicator);
                if (loadingDialog.isShowing()) loadingDialog.dismiss();
            }

            @Override
            public void onRequestSuccess(Call<PokedexResponse> call, Response<PokedexResponse> response) {
                ePokedexAdapter.updateDataset(response.body().getListadoResult());
                //int count = response.body().getListadoResult().size();
                //Picasso.get().load(response.body().getListadoResult().get(0).getUrl()).placeholder(R.color.black).error(R.color.black).into(imagePokedex);
            }

            @Override
            public void onRequestFailure(final Call<PokedexResponse> call, APIError error) {
                if (error.getStatusCode() == 401) {
                    DialogManager.showErrorDialog(mContext, error.getStatusMessage());
                } else {
                    showErrorDialog(RetrofitClient.buildErrorMessage(error));
                }
            }

            @Override
            public void onRequestError(final Call<PokedexResponse> call, Throwable t) {
                showErrorDialog(t.getMessage());
            }

            private void showErrorDialog(String s) {
                DialogManager.createDialog(mContext, getString(R.string.error), s)
                        .positiveText(R.string.try_again)
                        .negativeText(R.string.cancel)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick( MaterialDialog dialog, DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick( MaterialDialog dialog, DialogAction which) {
                                dialog.dismiss();
                            }
                        }).show();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, " - OnStart - ");

    }

    @Override
    public void onResume() {
        cargarPokedexService(mContext, PokedexFragment.this.getActivity().getIntent());
        super.onResume();
        Log.d(TAG, " - OnResume - ");

    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, " - OnPause - ");

    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, " - OnStop - ");

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, " - OnDestroy - ");
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onNotificationClick(String name, String url) {

        String[] parts = url.split("pokemon");
        String indice = parts[1].toString().replace("/","");
        String urlImage = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/"+ indice +".png";
        FragmentManager fragmentManager = PokedexFragment.this.getActivity().getSupportFragmentManager();
        Fragment fragment = PokedexDetailFragment.newInstance(urlImage, indice , name, "textoCompartir", 1);
        fragmentManager.beginTransaction().replace(R.id.fragment_container,fragment).addToBackStack("Pokedex").commit();
    }
}