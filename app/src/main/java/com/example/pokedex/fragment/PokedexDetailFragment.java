package com.example.pokedex.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.example.pokedex.R;
import com.example.pokedex.apiclient.RestServices;
import com.example.pokedex.apiclient.RetrofitCallback;
import com.example.pokedex.apiclient.RetrofitClient;
import com.example.pokedex.datamodel.gson.PokedexDetailResponse;
import com.example.pokedex.datamodel.gson.PokedexResponse;
import com.example.pokedex.datamodel.gson.adapter.ImageAdapter;
import com.example.pokedex.datamodel.gson.adapter.PokedexAdapter;
import com.example.pokedex.datamodel.gson.objects.APIError;
import com.example.pokedex.datamodel.gson.utils.DialogManager;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PokedexDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PokedexDetailFragment extends Fragment implements ImageAdapter.OnNotificationClickListener{

    private static final String TAG = "PokedexDetail";

    @BindView(R.id.textView_movimientos) TextView txtMovimientos;
    @BindView(R.id.textView_id) TextView txtID;
    @BindView(R.id.textView_nombre) TextView txtNombre;
    @BindView(R.id.textView_tipo) TextView txtTipo;
    @BindView(R.id.textView_peso) TextView txtPeso;
    @BindView(R.id.imageview_pokedex) ImageView imgPokedex;
    @BindView(R.id.textView_habilidades) TextView txtHabilidades;
    @BindView(R.id.recycler) RecyclerView recyclerView;


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";
    private static final int ARG_PARAM5 = 0;

    private Context mContext;
    private ImageAdapter eImageAdapter;

    // TODO: Rename and change types of parameters
    private String imagenPokedex;
    private String id;
    private String nombre;
    private String tipo;
    private int peso;
    private String espiritu;
    private List habilidades;
    private String movimientos;
    ArrayList<String> arrayList = new ArrayList<>();

    public PokedexDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PokedexDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PokedexDetailFragment newInstance(String param1, String param2, String param3, String param4, int param5) {
        PokedexDetailFragment fragment = new PokedexDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, param3);
        args.putString(ARG_PARAM4, param4);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();

        eImageAdapter = new ImageAdapter(mContext, new ArrayList<String>());

        if (getArguments() != null) {
            imagenPokedex = getArguments().getString(ARG_PARAM1);
            id = getArguments().getString(ARG_PARAM2);
            nombre = getArguments().getString(ARG_PARAM3);
            tipo = getArguments().getString(ARG_PARAM4);
            espiritu = getArguments().getString(ARG_PARAM4);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pokedex_detail, container, false);
        ButterKnife.bind(this, view);

        Picasso.with(mContext).load(imagenPokedex).placeholder(R.color.mdtp_transparent_black).error(R.color.mdtp_transparent_black).into(imgPokedex);

        txtID.setText("ID: " + id);
        txtNombre.setText("Nombre: " + nombre);
        /*
        txtTipo.setText("Tipo: "+tipo);
        txtPeso.setText("Peso: " + peso);
        txtEspiritu.setText("Espíritu: "+espiritu);
        txtHabilidades.setText("Habilidades: " + espiritu);
        txtMovimientos.setText("Movimientos: " + espiritu);
        */

        return view;
    }

    private void cargarDetailPokedexService(final Context context, final Intent intent) {
        final MaterialDialog loadingDialog = DialogManager.showLoadingDialog(getActivity()).build();
        final Handler runLoadingIndicator = new Handler();
        final Runnable showLoadingIndicator = new Runnable() {
            @Override
            public void run() {
                loadingDialog.show();
            }
        };

        RestServices restServices = RetrofitClient.getInstance().setConnection(RestServices.class);
        Call<PokedexDetailResponse> call = restServices.getDetailPokemon(Integer.parseInt(id));
        call.enqueue(new RetrofitCallback<PokedexDetailResponse>() {
            @Override
            public void onStart() {
                runLoadingIndicator.postDelayed(showLoadingIndicator, 600);
            }

            @Override
            public void onFinish() {
                runLoadingIndicator.removeCallbacks(showLoadingIndicator);
                if (loadingDialog.isShowing()) loadingDialog.dismiss();
            }

            @Override
            public void onRequestSuccess(Call<PokedexDetailResponse> call, Response<PokedexDetailResponse> response) {
                //tipo = response.body().getResult();
                /*
                peso = ;
                espiritu = ;
                habilidades = ;
                movimientos = ;
                 */
                try {
                    String abilities = "";
                    String types = "";
                    for (int i = 0; i < response.body().getListAbilities().size(); i++) {
                        if (i == 0) {
                            abilities = response.body().getListAbilities().get(i).getAbility().getName();
                        } else {
                            abilities = abilities + ", " + response.body().getListAbilities().get(i).getAbility().getName();
                        }

                    }
                    txtHabilidades.setText("Habilidades: " + abilities);
                    for (int i = 0; i < response.body().getListTypes().size(); i++) {
                        if (i == 0) {
                            types = response.body().getListTypes().get(i).getType().getName();
                        } else {
                            types = types + ", " + response.body().getListTypes().get(i).getType().getName();
                        }

                    }
                    txtTipo.setText("Tipos: "+types);
                    peso = response.body().getWeight();
                    txtPeso.setText("Peso: " + peso);
                    String ima0 = response.body().getSprites().getBackDefault();
                    arrayList.add(ima0);
                    //Picasso.with(mContext).load(ima0).placeholder(R.color.mdtp_transparent_black).error(R.color.mdtp_transparent_black).into(imageView0);
                    String ima1 = response.body().getSprites().getBackShiny();
                    arrayList.add(ima1);
                    //Picasso.with(mContext).load(ima1).placeholder(R.color.mdtp_transparent_black).error(R.color.mdtp_transparent_black).into(imageView1);
                    String ima2 = response.body().getSprites().getFrontDefault();
                    arrayList.add(ima2);
                    //Picasso.with(mContext).load(ima2).placeholder(R.color.mdtp_transparent_black).error(R.color.mdtp_transparent_black).into(imageView2);
                    String ima3 = response.body().getSprites().getFrontShiny();
                    arrayList.add(ima3);
                    //Picasso.with(mContext).load(ima3).placeholder(R.color.mdtp_transparent_black).error(R.color.mdtp_transparent_black).into(imageView3);
                    //ImageAdapter adapter = new ImageAdapter(mContext, arrayList);
                    eImageAdapter = new ImageAdapter(mContext, arrayList);
                    recyclerView.setAdapter(eImageAdapter);
                    //eImageAdapter.updateDataset(arrayList);

                }
                catch (Exception ex){
                    Log.d(TAG, ex.getMessage());
                }
            }

            @Override
            public void onRequestFailure(final Call<PokedexDetailResponse> call, APIError error) {
                if (error.getStatusCode() == 401) {
                    DialogManager.showErrorDialog(mContext, error.getStatusMessage());
                } else {
                    showErrorDialog(RetrofitClient.buildErrorMessage(error));
                }
            }

            @Override
            public void onRequestError(final Call<PokedexDetailResponse> call, Throwable t) {
                showErrorDialog(t.getMessage());
            }

            private void showErrorDialog(String s) {
                DialogManager.createDialog(mContext, getString(R.string.error), s)
                        .positiveText(R.string.try_again)
                        .negativeText(R.string.cancel)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NotNull MaterialDialog dialog, @NotNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NotNull MaterialDialog dialog, @NotNull DialogAction which) {
                                dialog.dismiss();
                            }
                        }).show();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, " - OnStart - ");

    }

    @Override
    public void onResume() {
        cargarDetailPokedexService(mContext, PokedexDetailFragment.this.getActivity().getIntent());
        super.onResume();
        Log.d(TAG, " - OnResume - ");

    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, " - OnPause - ");

    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, " - OnStop - ");

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, " - OnDestroy - ");
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onNotificationClick(String name, String url) {

    }
}